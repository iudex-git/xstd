/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <xstd/bitmap.hxx>

Optional<bool> Bitmap::get(u64 index) {
	if(index > this->buffer_size) return None(bool);

	u64 byteIndex = index / 8;
	byte bitIndex = index % 8;
	byte bitIndexer = 0b10000000 >> bitIndex;

	bool value = (this->buffer[byteIndex] & bitIndexer) > 0;
	return Some(bool, value);
}

Optional<bool> Bitmap::set(u64 index, bool value) {
	if(index > this->buffer_size) return None(bool);

	u64 byteIndex = index / 8;
	byte bitIndex = index % 8;
	byte bitIndexer = 0b10000000 >> bitIndex;

	bool old_val = (buffer[byteIndex] & bitIndexer) > 0;

	this->buffer[byteIndex] &= ~bitIndexer;

	if(value) {
		this->buffer[byteIndex] |= bitIndexer;
	}

	return Some(bool, old_val);
}
