/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <memory.h>

void* memset(void* dst, int val, size_t len) {
	unsigned char *ptr = (unsigned char*) dst;

	while(len-- > 0 && len != SIZE_MAX) {
		*ptr++ = val;
	}

	return dst;
}

void memcpy(void* dst, void* src, size_t len) {
	unsigned char *pdst = (unsigned char*) dst;
	unsigned char *psrc = (unsigned char*) src;

	for(size_t i = 0; i < len; i++) {
		pdst[i] = psrc[i];
	}
}
