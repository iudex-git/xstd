/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <stdio.h>
#include <printf/printf.h>

int putchar(int c) {
	putchar_((char) c);
	return c;
}

int printf(const char* format, ...) {
	va_list va;
	va_start(va, format);

	int ret = vprintf_(format, va);

	va_end(va);
	return ret;
}

int vprintf(const char* format, va_list arg) {
	return vprintf_(format, arg);
}

int sprintf(char* s, const char* format, ...) {
	va_list va;
	va_start(va, format);

	int ret = vsprintf_(s, format, va);

	va_end(va);
	return ret;
}

int vsprintf(char* s, const char* format, va_list arg) {
	return vsprintf_(s, format, arg);
}

int snprintf(char* s, size_t count, const char* format, ...) {
	va_list va;
	va_start(va, format);

	int ret = vsnprintf_(s, count, format, va);

	va_end(va);
	return ret;
}

int vsnprintf(char* s, size_t count, const char* format, va_list arg) {
	return vsnprintf_(s, count, format, arg);
}
