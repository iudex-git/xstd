/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <string.h>

char* strcat(char* dst, const char* src) {
	char* rdst = dst;

	while(*dst) dst++;
	while((*dst++ = *src++));
	
	return rdst;
}

size_t strlen(const char* s) {
	return (*s) ? strlen(++s) + 1 : 0;
}

size_t strcmp(const char* s1, const char* s2) {
	while(*s1 && (*s1 == *s2)) {
		s1++;
		s2++;
	}

	return *(const unsigned char*) s1 - *(const unsigned char*) s2;
}
