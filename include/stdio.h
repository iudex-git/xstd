/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <stdarg.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif
	/* this function needs to be implemented */
	void putchar_(char c);

	int putchar(int c);

	int printf(const char* format, ...);
	int vprintf(const char* format, va_list arg);
	int sprintf(char* s, const char* format, ...) ;
	int vsprintf(char* s, const char* format, va_list arg);
	int snprintf(char* s, size_t count, const char* format, ...);
	int vsnprintf(char* s, size_t count, const char* format, va_list arg);
#ifdef __cplusplus
}
#endif
