/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <types.h>

#ifdef __cplusplus
extern "C" {
#endif
	void* memset(void *dst, int val, size_t len);
	void memcpy(void* dst, void* src, size_t len);
#ifdef __cplusplus
}
#endif
