/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#define None(T) Optional<T>()
#define Some(T, v) Optional<T>(&v)

template<typename T>
class Optional {
private:
	bool has_value;
	T value;
public:
	Optional() : has_value(false) { }
	Optional(T* value) : has_value(value != nullptr) {
		if(value != nullptr) this->value = *value;
	}
	
	inline bool has() { return has_value; }

	inline T* get() {
		if(has()) return &value;
		else return nullptr;
	}

	template<typename F>
	inline T* get_and(F func) {
		T* t = get();

		if(t != nullptr) func(t);
		return t;
	}

	template<typename F>
	inline T* get_or(F func) {
		T* t = get();

		if(t == nullptr) func(t);
		return t;
	}

	template<typename F1, typename F2>
	inline T* get_and_or(F1 func1, F2 func2) {
		T* t = get();

		if(t == nullptr) func2(t);
		else func1(t);

		return t;
	}
};
