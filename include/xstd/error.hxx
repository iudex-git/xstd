/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <types.h>
#include <string.h>

#define ERROR(code, msg) Error(code, \
	__FILE__ STRINGIFY(:__LINE__), msg)

class Error {
private:
	size_t code;
	const char* source;
	const char* message;
public:
	Error(size_t code, const char* source, const char* message)
		: code(code), source(source), message(message) { }
	
	Error(size_t code, const char* message)
		: code(code), source("<unknown>"), message(message) { }
	
	Error(size_t code)
		: code(code), source("<unknown>"), message("") { }
	
	Error() : code(0), source("<unknown>"), message("") { }

	inline size_t get_code() {
		return code;
	}

	inline const char* get_source() {
		return source;
	}

	inline const char* get_message() {
		return message;
	}
};
