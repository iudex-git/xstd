/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <types.h>
#include <xstd/optional.hxx>

class Bitmap {
private:
	byte* buffer;
	u64 buffer_size;
public:
	u64 size;
	
	Bitmap(byte* buffer, u64 size)
		: buffer(buffer), buffer_size(size / 8), size(size) { }

	Optional<bool> get(u64 index);
	Optional<bool> set(u64 index, bool value);
};
