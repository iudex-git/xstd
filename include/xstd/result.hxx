/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <xstd/optional.hxx>

#define SUCCESS(O, E, v) Result<O, E>(true, v, nullptr)
#define FAILURE(O, E, v) Result<O, E>(false, nullptr, v)

struct Empty { };

template<typename O, typename E>
class Result {
private:
	bool result;
	
	Optional<O> ok_value;
	Optional<E> error_value;
public:
	Result(bool result, O* ok_value, E* error_value) : result(result) {
		if(result) this->ok_value = Optional<O>(ok_value);
		else this->error_value = Optional<E>(error_value);
	}
	
	inline bool is_ok() { return result; }
	inline bool is_err() { return !result; }
	
	inline O* get_value() {
		if(is_ok() && ok_value.has()) return ok_value.get();
		else return nullptr;
	}
	
	inline E* get_error() {
		if(is_err() && error_value.has()) return error_value.get();
		else return nullptr;
	}
};
