/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <types.h>

#define _STRINGIFY(x) #x
#define STRINGIFY(x) _STRINGIFY(x)

#ifdef __cplusplus
extern "C" {
#endif
	char* strcat(char* dst, const char* src);
	size_t strlen(const char* s);
	size_t strcmp(const char* s1, const char* s2);
	void swap(char* a, char* b);
	void reverse(char* s, int len);
	char* itoa(int n, char* s, int base);
#ifdef __cplusplus
}
#endif
